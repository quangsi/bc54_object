var user1 = {
  // key:value,

  name: "alice",
  phone: "28884567",
  email: "alice@gmail.com",
};

console.log("😀 - user1", user1.name);
var user2 = {
  // property
  name: "Bob",
  phone: "1231233",
  email: "Bob@gmail.com",
  //   method
  sayHello: function () {
    // this dùng bên trong ojbect để truy xuất giá trị của object
    console.log("hello hello toi ten la: ", this.name);
  },
};
user2.sayHello();
user2.name = "Bob Nguyen";
console.log("😀 - user2", user2);

// user2.sayHello();
/**
 * array: [] , index => lưu dữ liệu cùng loại theo danh sách
 *
 * object: {} , key => lưu dữ liệu miêu tả chung 1 đối tượng
 *
 */
