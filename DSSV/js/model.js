var dog1 = {
  name: "lulu",
  age: 2,
};

var dog2 = {
  ten: "milo",
  tuoi: 3,
};

function Dog(_name, _age, _color) {
  this.name = _name;
  this.age = _age;
  this.color = _color;
}
var dog3 = new Dog("meomeo", 2, "black");
var dog4 = new Dog("mimi", 2, "white");
console.log("😀 - dog4", dog4);
console.log("😀 - dog3", dog3);

function SinhVien(ma, ten, matKhau, email, toan, ly, hoa) {
  this.ma = ma;
  this.ten = ten;
  this.matKhau = matKhau;
  this.email = email;
  this.toan = toan;
  this.ly = ly;
  this.hoa = hoa;
  this.tinhDTb = function () {
    var dtb = (this.toan + this.ly + this.hoa) / 3;
    return dtb;
  };
}

var nums = [2, 4, 6, 8];

var newNums = nums.map(function (item) {
  return item + "yes";
});
console.log("😀 - newNums - newNums", newNums);
